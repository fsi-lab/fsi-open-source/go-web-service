package trace

import (
	"fmt"
	"log"
	"runtime"
)

func Trace() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return fmt.Sprintf("file=%s:%d func=%s", frame.File, frame.Line, frame.Function)
}

func LogError(err string, trace string) {
	log.Printf("level=error %s err=%s", trace, err)
}
