package main

// @title Web Service Example API
// @version 1.0
// @termsOfService http://swagger.io/terms/
// @host localhost:8082
// @BasePath /

// @securityDefinitions.basic  BasicAuth
// @in header
// @name Authorization

import (
	"context"
	"fmt"
	"gitlab.com/fsi-lab/fsi-open-source/go-pkgz/utils/storage"
	webserver "go-web-service/cmd/web-server"
	"log"
	"os"
)

var version = "0.0.0"

func main() {
	fmt.Printf("Started, v.%s", version)
	port := os.Getenv("SERVICE_PORT")
	if port == "" {
		port = "8082"
	}
	log.Printf("[DEBUG] Open API documentation: %s", "http://localhost:"+port+"/swagger/index.html")

	mainCtx, cancel := context.WithCancel(context.Background())
	ctx := context.WithValue(mainCtx, "version", version)

	// Get storage ------------------------------------------------------------------
	db, err := storage.NewKV("local")
	if err != nil {
		panic(err)
	}

	routerOptions := webserver.RouterOptions{
		Port: port,
		Services: []webserver.WebService{
			{Path: "/api/v1/storage", Router: db.GetRouter()},
		},
	}

	_ = webserver.Route(ctx, routerOptions)
	cancel()
}
