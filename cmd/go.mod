module go-web-service/cmd

go 1.17

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.1
	github.com/swaggo/http-swagger v1.3.3
	github.com/swaggo/swag v1.8.1
	gitlab.com/fsi-lab/fsi-open-source/go-pkgz/auth v0.0.0-20220915140544-6c1162dc785d
	gitlab.com/fsi-lab/fsi-open-source/go-pkgz/utils v0.0.0-20220915144609-fc663ca4cab2
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/go-openapi/jsonpointer v0.19.5 // indirect
	github.com/go-openapi/jsonreference v0.20.0 // indirect
	github.com/go-openapi/spec v0.20.6 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/swaggo/files v0.0.0-20220610200504-28940afbdbfe // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/net v0.0.0-20220425223048-2871e0cb64e4 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/tools v0.1.10 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
