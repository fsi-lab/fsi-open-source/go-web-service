package web_server

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"io/ioutil"
	"log"
	"net/http"
)

// NewBaseController baseController serves Home web page
func NewBaseController() func(r chi.Router) {
	return func(r chi.Router) {
		r.Post("/api/v1/tracking", trackingHandler)
		r.Get("/", homeHandler)
	}
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.Write(getIndexHtml())
}

func getIndexHtml() []byte {
	file, err := ioutil.ReadFile(indexFilePath)
	if err != nil {
		panic("[FATAL] index.html file not found")
	}
	return file
}

type TrackingInfo struct {
	Tag  string `json:"tag"`
	Path string `json:"path"`
	Hash string `json:"Hash"`
	Site string `json:"site"`
}

func trackingHandler(w http.ResponseWriter, r *http.Request) {
	var trackingInfo TrackingInfo
	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&trackingInfo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	log.Printf("tracking info=tracking tag=%s path=%s site=%s", trackingInfo.Tag, trackingInfo.Path, trackingInfo.Site)
	w.WriteHeader(http.StatusNoContent)
}
