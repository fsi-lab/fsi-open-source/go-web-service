package web_server

import (
	"github.com/go-chi/cors"
)

var corsMiddleware = cors.New(cors.Options{
	AllowedOrigins:   []string{"http://localhost:1323", "localhost:3000", "http://localhost:8081"},
	AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-XSRF-TOKEN", "X-JWT"},
	ExposedHeaders:   []string{"Authorization"},
	AllowCredentials: true,
	MaxAge:           300,
})
