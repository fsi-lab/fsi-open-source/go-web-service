package web_server

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	httpSwagger "github.com/swaggo/http-swagger"
	auth_client "gitlab.com/fsi-lab/fsi-open-source/go-pkgz/auth/pkg/go-client"
	_ "go-web-service/cmd/docs"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

var indexFilePath = "web/dist/index.html"
var staticPath = "web/dist/"
var redirectURL string

type RouterOptions struct {
	Port     string
	Services []WebService
}

type WebService struct {
	Router http.Handler
	Path   string
}

var ctx context.Context

func Route(c context.Context, opt RouterOptions) error {
	ctx = c

	router := chi.NewRouter()
	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	//router.Use(middleware.Logger)
	router.Use(middleware.Timeout(60 * time.Second))
	router.Use(middleware.Compress(5))
	router.Use(corsMiddleware.Handler)

	router.Get("/about", aboutHandler)

	// Mount routes available for anonymous users ------------------------------------------------------------
	for _, s := range opt.Services {
		log.Printf("level=DEBUG tag=web-server mount=%s", s.Path)
		router.Mount(s.Path, s.Router)
	}

	var baseController = NewBaseController()
	router.Route("/", baseController)

	a := auth_client.NewClient("http://localhost:8117")
	router.Mount("/api/v1/user/", a)
	router.With(a.Auth).Get("/private", aboutHandler)
	router.With(a.Auth, a.Admin).Get("/admin", aboutHandler)

	// Handle static assets ----------------------------------------------------------------------------------
	workDir, _ := os.Getwd()
	filesDir := http.Dir(filepath.Join(workDir, "web/dist/static"))
	FileServer(router, "/static", filesDir)
	assetsDir := http.Dir(filepath.Join(workDir, "web/dist/assets"))
	FileServer(router, "/assets", assetsDir)

	// SWAGGER DOCS
	sw := chi.NewRouter()
	sw.Get("/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:"+opt.Port+"/swagger/doc.json"),
	))
	router.Mount("/swagger", sw)

	// Start HTTPS server ------------------------------------------------------------------------------------
	http.Handle("/", router)
	server := &http.Server{Addr: fmt.Sprintf(":%s", opt.Port)}
	log.Printf("level=INFO Listening on port=%s", opt.Port)
	go func() {
		<-ctx.Done()
		log.Printf("level=INFO shutting down HTTP server ...")
		server.Shutdown(ctx)
	}()
	return server.ListenAndServe()
}

func aboutHandler(w http.ResponseWriter, r *http.Request) {
	version := ctx.Value("version")
	w.Write([]byte(fmt.Sprintf(`{ "version": "%s" }`, version)))
}
